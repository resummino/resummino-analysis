# Resummino Analysis Tool

This is a small tool to analyze output by [Resummino](http://www.resummino.org/). It can be used to compute scale and PDF uncertainties for total cross sections as well as distributions from the output files created by running Resummino with the `output_file` option.

## Usage

To use resummino_analysis you first need to run Resummino using the `output_file` parameter. For example to obtain a transverse momentum distribution with scale errors you could use something like:

    $ for pt in $(seq 10 5 100); do
    $ for scale in $(seq 0.25 0.5 1); do
    $ resummino --mu_f=$scale --mu_r=$scale --pt=$pt --output_file=scale_${scale}_pt_${pt}.out input_file.in
    $ done
    $ done

In real life you would probably run those processes in parallel in a cluster—that is why these loops are not included in Resummino itself.

Once you have all the `*.out` files, you can use analysis script by issuing:

    $ python resummino_analysis.py *.out

You will obtain a table with the distribution and the corresponding uncertainties (in this case only scale uncertainty, but resummino_analysis will also compute PDF uncertainties if you run the corresponding variations in a similar way to the previous example).

## Legal and contact

This is open source software under the terms of the European Union Public Licence (EUPL), version 1.1 or later.

You can find more information at the Resummino website <http://www.resummino.org/>. If you find any issue or have any suggestion please let us know.
